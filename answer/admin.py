from django.contrib import admin
from answer.models import Answer


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('user', 'question_name', 'value', 'index', )
    fields = ('user', 'question_name', 'value', 'index', )
    readonly_fields = ('index', )
