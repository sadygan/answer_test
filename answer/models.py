from django.db import models
from django.contrib.auth.models import User


class Answer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='answers', blank=False, null=False, )
    question_name = models.CharField(max_length=255)
    value = models.TextField()
    index = models.PositiveIntegerField(default=0)

    def save(self, *args, **kwargs):
        # if the object is created, then we update it with the current index
        if self.pk is None:
            qs = Answer.objects.filter(question_name=self.question_name).\
                values_list('index', flat=True)

            # maybe in db objects the same index
            uniq_indexes = set(qs)
            if uniq_indexes:
                _index = 0
                for i in uniq_indexes:
                    if i == _index:
                        _index += 1
                self.index = _index

        super(Answer, self).save(*args, **kwargs)

    def __str__(self):
        return '{0} {1} {2} {3} {4}'.format(self.id, self.user, self.question_name,
                                        self.value, self.index)
