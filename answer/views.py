from django.http import HttpResponse
from libs.PDFlib.PDFlib import *
from .models import Answer

def render_pdf(request, user_id):
    answers = Answer.objects.filter(user=user_id)
    try:
        case_number = answers.filter(question_name__iexact='case_number')[0].value
    except:
        case_number = ""
    your_name = answers.filter(question_name__iexact='your_name')[0].value

    try:
        spuse_name = answers.filter(question_name__iexact='spuse_name')[0].value
    except:
        spuse_name = ""

    try:
        court_number = answers.filter(question_name__iexact='court_number')[0].value
    except:
        court_number = ""

    try:
        county = answers.filter(question_name__iexact='county')[0].value
    except:
        county = ""

    # create a new PDFlib objects
    p = PDFlib()

    # This means we must check return values of load_font() etc.
    p.set_option("errorpolicy=return")

    p.begin_document("", "")

    p_left = 70
    xl = 595
    yl = 842

    MAX_INTEREST_OF = 6

    font = p.load_font("Helvetica", "unicode", "")
    if font == -1:
        raise PDFlibException("Error: " + p.get_errmsg())

    optlist1 = "fontname=Helvetica fontsize=10 encoding=unicode"
    optlist2 = "fontname=Helvetica-Bold fontsize=10 encoding=unicode"
    optlist3 = "fontname=Helvetica-Bold fontsize=9 encoding=unicode"
    optlist4 = "fontname=Helvetica fontstyle=italic fontsize=7 encoding=unicode"
    optlist5 = "fontname=Helvetica fontsize=9 encoding=unicode"

    p.begin_page_ext(xl, yl, "")

    p.setfont(font, 10)
    p.set_text_pos(p_left, yl-80)
    p.show("NOTICE: THIS DOCUMENT CONTAINS SENSITIVE DATA.")
    p.fit_textline("Cause Number: ________________________", xl/2, yl-140, "position={center top}")
    p.fit_textline(case_number, xl/2, yl-145, optlist3)

    p.fit_textline("IN THE MATTER OF MARRIAGE OF", p_left, yl-170, optlist2)
    p.fit_textline("Petitioner: ___________________________________________",
                   p_left, yl-190, optlist3)
    p.fit_textline(your_name, p_left+70, yl-190, optlist3)
    p.fit_textline("Print first, middle and last name of the spouse who filed for divorce.",
                   p_left+45, yl-200, optlist4)
    p.fit_textline("And", p_left+140, yl-225, optlist1)
    p.fit_textline("Respondent: _________________________________________",
                   p_left, yl-250, optlist3)
    p.fit_textline(spuse_name, p_left+70, yl-250, optlist3)
    p.fit_textline("Print first, middle and last name of the other spouse.",
                   p_left+60, yl-260, optlist4)
    p.fit_textline("AND IN THE INTEREST OF:", p_left, yl-280, optlist2)

    p.fit_textline("In the _____________", p_left+290, yl-175, optlist5)
    p.fit_textline(court_number, p_left+320, yl-175, optlist3)
    p.fit_textline("(Court Number)", p_left+320, yl-185, optlist4)
    p.fit_textline("Counry Court at Law", p_left+305, yl-228, optlist1)
    p.fit_textline("_____________ County, Texas", p_left+290, yl-250, optlist1)
    p.fit_textline(county, p_left+290, yl-250, optlist3)

    # Vertical line
    p.set_graphics_option("strokecolor=black")
    p.moveto(xl-250, yl-160)
    p.lineto(xl-250, yl-260)
    p.stroke()

    # rect one
    p.rect(xl-235, yl-210, 10, 10)
    p.stroke()
    p.fit_textline("District Court", p_left+305, yl-208, optlist1)

    # rect two
    p.rect(xl-235, yl-230, 10, 10)
    p.stroke()

    #  Draw childrens form
    count = 0
    step_x = 0
    step_y = 0
    while count < MAX_INTEREST_OF:
        if count % 3 == 0:
            step_y += 20
            step_x = 0
        count += 1
        p.fit_textline(str(count) + "._________________________", p_left+step_x, yl-270-step_y, "position={left top}")
        step_x += 155

    count = 0
    step_x = 0
    step_y = 0
    for answer in answers:
        if answer.question_name.lower() == 'children':
            if count % 3 == 0:
                step_y += 20
                step_x = 0
            count += 1
            p.fit_textline(answer.value, p_left+10+step_x, yl-270-step_y, "position={left top}")
            step_x += 155

    p.end_page_ext("")

    p.end_document("")

    buf = p.get_buffer()
    p.delete();
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(buf, content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="somefilename.pdf"'
    return response
